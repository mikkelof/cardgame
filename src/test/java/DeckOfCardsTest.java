import edu.ntnu.idatt2001.oblig3.mikkelof.DeckOfCards;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {

    @Test
    @DisplayName("Creating a new deck of cards")
    public void createANewDeckOfCards() {
        DeckOfCards deck = new DeckOfCards();
        Assertions.assertEquals(deck.cards.size(), 52);
    }

    @Nested
    @DisplayName("Tries dealing hands with different sizes")
    class dealHand {
        DeckOfCards deck = new DeckOfCards();
        @Test
        public void shouldDealHandWithFiveCards() {
            Assertions.assertEquals(deck.dealHand(5).getHand().size(), 5);
        }
        @Test
        public void shouldDealHandWithTwentyCards() {
            Assertions.assertEquals(deck.dealHand(20).getHand().size(), 20);
        }
    }
}
