import edu.ntnu.idatt2001.oblig3.mikkelof.HandOfCards;
import edu.ntnu.idatt2001.oblig3.mikkelof.PlayingCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class HandOfCardsTest {
    @Test
    @DisplayName("Tests adding a playingcard to a hand")
    public void addNewCardToHand() {
        HandOfCards hand = new HandOfCards();
        HandOfCards hand2 = new HandOfCards();
        PlayingCard newCard = new PlayingCard('H', 8);
        hand.addCard(newCard);
        Assertions.assertEquals(hand.getHand().size(), hand2.getHand().size() + 1);
    }

    @Test
    @DisplayName("Tests the findSum method")
    public void findSumOfAHand() {
        PlayingCard newCard1 = new PlayingCard('H', 3);
        PlayingCard newCard2 = new PlayingCard('H', 4);
        PlayingCard newCard3 = new PlayingCard('H', 5);
        PlayingCard newCard4 = new PlayingCard('H', 6);
        PlayingCard newCard5 = new PlayingCard('H', 7);
        HandOfCards hand = new HandOfCards();
        hand.addCard(newCard1);
        hand.addCard(newCard2);
        hand.addCard(newCard3);
        hand.addCard(newCard4);
        hand.addCard(newCard5);
        Assertions.assertEquals(hand.findSum(), 25);
    }

    @Test
    @DisplayName("Tests the filterHearts method")
    public void filterHeartsMethod() {
        PlayingCard newCard1 = new PlayingCard('H', 3);
        PlayingCard newCard2 = new PlayingCard('H', 4);
        PlayingCard newCard3 = new PlayingCard('H', 5);
        PlayingCard newCard4 = new PlayingCard('S', 6);
        PlayingCard newCard5 = new PlayingCard('S', 7);
        HandOfCards hand = new HandOfCards();
        hand.addCard(newCard1);
        hand.addCard(newCard2);
        hand.addCard(newCard3);
        hand.addCard(newCard4);
        hand.addCard(newCard5);
        Assertions.assertEquals(hand.filterHearts().size(), 3);
    }

    @Nested
    @DisplayName("Tests the checkForSpadesQueen method")
    class checkForSpadesQueen {
        @Test
        public void checkForQueenOfSpadesWhenItExists() {
            PlayingCard newCard1 = new PlayingCard('H', 3);
            PlayingCard newCard2 = new PlayingCard('H', 4);
            PlayingCard newCard3 = new PlayingCard('H', 5);
            PlayingCard newCard4 = new PlayingCard('S', 6);
            PlayingCard newCard5 = new PlayingCard('S', 12);
            HandOfCards hand = new HandOfCards();
            hand.addCard(newCard1);
            hand.addCard(newCard2);
            hand.addCard(newCard3);
            hand.addCard(newCard4);
            hand.addCard(newCard5);
            Assertions.assertTrue(hand.checkForSpadesQueen());
        }

        @Test
        public void checkForQueenOfSpadesWhenItDoesNotExists() {
            PlayingCard newCard1 = new PlayingCard('H', 3);
            PlayingCard newCard2 = new PlayingCard('H', 4);
            PlayingCard newCard3 = new PlayingCard('H', 5);
            PlayingCard newCard4 = new PlayingCard('S', 6);
            PlayingCard newCard5 = new PlayingCard('S', 7);
            HandOfCards hand = new HandOfCards();
            hand.addCard(newCard1);
            hand.addCard(newCard2);
            hand.addCard(newCard3);
            hand.addCard(newCard4);
            hand.addCard(newCard5);
            Assertions.assertFalse(hand.checkForSpadesQueen());
        }
    }

    @Nested
    @DisplayName("Tests the checkFlush method")
    class checkFlush {
        @Test
        public void checkForFlushWhenItExists() {
            PlayingCard newCard1 = new PlayingCard('H', 3);
            PlayingCard newCard2 = new PlayingCard('H', 4);
            PlayingCard newCard3 = new PlayingCard('H', 5);
            PlayingCard newCard4 = new PlayingCard('H', 6);
            PlayingCard newCard5 = new PlayingCard('H', 12);
            HandOfCards hand = new HandOfCards();
            hand.addCard(newCard1);
            hand.addCard(newCard2);
            hand.addCard(newCard3);
            hand.addCard(newCard4);
            hand.addCard(newCard5);
            Assertions.assertTrue(hand.checkFlush());
        }

        @Test
        public void checkForFlushWhenItDoesNotExists() {
            PlayingCard newCard1 = new PlayingCard('H', 3);
            PlayingCard newCard2 = new PlayingCard('H', 4);
            PlayingCard newCard3 = new PlayingCard('H', 5);
            PlayingCard newCard4 = new PlayingCard('S', 6);
            PlayingCard newCard5 = new PlayingCard('S', 7);
            HandOfCards hand = new HandOfCards();
            hand.addCard(newCard1);
            hand.addCard(newCard2);
            hand.addCard(newCard3);
            hand.addCard(newCard4);
            hand.addCard(newCard5);
            Assertions.assertFalse(hand.checkFlush());
        }
    }
}
