package edu.ntnu.idatt2001.oblig3.mikkelof;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a hand of cards. A deck is made up of n cards
 *
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class HandOfCards {
    private ArrayList<PlayingCard> handOfCards = new ArrayList<>();

    /**
     * Adds a new card to the hand
     * @param playingCard The card you want to add to the hand
     */

    public void addCard(PlayingCard playingCard) {
        this.handOfCards.add(playingCard);
    }

    /**
     * Checks if the hand contains a given card
     * @param playingCard The card you want to check if the hand contains
     * @return True if the hand contains the given card, False if not
     */

    public boolean handContains(PlayingCard playingCard) {
        return this.handOfCards.contains(playingCard);
    }

    /**
     * A method for getting all the cards in the hand as an ArrayList
     * @return An ArrayList of all the cards in the hand
     */

    public ArrayList<PlayingCard> getHand() {
        return handOfCards;
    }

    /**
     * Finds the sum of the faces of all the cards in the hand
     * @return The sum of all the cards in the hand as an integer
     */

    public int findSum(){
        return handOfCards.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * Filters the method and only keeps the card who are of the heart suit
     * @return a list of all the cards in the hand who are of the heart suit
     */

    public List<PlayingCard> filterHearts() {
        return handOfCards.stream().filter(s -> s.getSuit() == 'H').collect(Collectors.toList());
    }

    /**
     * Checks if the hand has the queen of spades
     * @return True if the hand has the queen of spades, False if not
     */

    public boolean checkForSpadesQueen() {
        return handOfCards.stream().anyMatch(q -> q.getSuit() == 'S' && q.getFace() == 12);
    }

    /**
     * Checks if all the cards in a hand are of the same suit, aka a flush. Checks three of the suits first
     * and if none of them are true it checks and returns the result from the last suit. This is as if
     * it reaches the last suit it will be the only suit that matters, and the result from that suit
     * will represent the entire hand
     * @return True if all the cards in the hand are of the same suit, False if not
     */

    public boolean checkFlush() {
        if (handOfCards.stream().allMatch(f -> f.getSuit() == 'S')) {
            return true;
        }
        else if (handOfCards.stream().allMatch(f -> f.getSuit() == 'H')) {
            return true;
        }
        else if (handOfCards.stream().allMatch(f -> f.getSuit() == 'D')) {
            return true;
        }
        else return (handOfCards.stream().allMatch(f -> f.getSuit() == 'C'));
    }
}
