package edu.ntnu.idatt2001.oblig3.mikkelof;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Controller for the fxml file/GUI
 *
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class Controller {
    private DeckOfCards deckOfCards = new DeckOfCards();
    private HandOfCards handOfCards = new HandOfCards();
    private int n = 5;

    @FXML
    public Button dealHand;
    @FXML
    public Label sumDisplay;
    @FXML
    public Button checkHandButton;
    @FXML
    public Label cardDisplay;
    @FXML
    public Label flushDisplay;
    @FXML
    public Label heartDisplay;
    @FXML
    public Label spadesQueenDisplay;

    public void dealHand() {
        handOfCards = deckOfCards.dealHand(n);

        cardDisplay.setText(handOfCards.getHand().toString());
    }

    public void checkHand() {
        sumDisplay.setText(String.valueOf(handOfCards.findSum()));

        if (handOfCards.checkFlush()) {
            flushDisplay.setText("Yes");
        }
        else {
            flushDisplay.setText("No");
        }

        if (handOfCards.filterHearts().size() == 0) {
            heartDisplay.setText("No Hearts");
        }
        else {
            heartDisplay.setText(handOfCards.filterHearts().toString());
        }

        if (handOfCards.checkForSpadesQueen()) {
            spadesQueenDisplay.setText("Yes");
        }
        else {
            spadesQueenDisplay.setText("No");
        }
    }
}
