package edu.ntnu.idatt2001.oblig3.mikkelof;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a deck of cards. A deck is made up of 52 cards, 13 of each suit
 *
 * @author Mikkel Ofrim
 * @version 1.0.0
 */

public class DeckOfCards {

    public final ArrayList<PlayingCard> cards = new ArrayList<>();

    /**
     * S = Spade, H = Heart, D = Diamonds, C = Clubs
     */

    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private Random rand = new Random();
    HandOfCards hand = new HandOfCards();

    /**
     * Creates a new full deck of cards
     */

    public DeckOfCards() {
        for (char c : suit) {
            for (int i = 1; i <= 13; i++) {
                cards.add(new PlayingCard(c, i));
            }
        }
    }

    //Maybe change to a method that removes a card from the deck when its drawn
    //And a new method to start a new fresh deck

    /**
     * Return a random hand of n cards when called. Checks if the user input is less than 1 or more than 52 as
     * these are invalid inputs considering a deck consists of 52 cards and a hand can not be 0 or negative cards.
     * Then it draws a random card from the cards ArrayList and adds it to the hand if it is not already in
     * the hand
     *
     * @param n An integer for the amount of cards you want to be dealt to the hand
     * @return The random hand that has been dealt
     */

    public HandOfCards dealHand(int n){
        HandOfCards localHand = new HandOfCards();
        PlayingCard newCard;
        if (n > 52 || n < 1){
            //Should make an exception instead
            throw new IllegalArgumentException("Dealt hand can not be smaller than 1 or bigger than 52 cards");
        }
        int i = 0;
        while(n > i){
            newCard = cards.get(rand.nextInt(52));
            if(localHand.handContains(newCard)){
                continue;
            }
            else{
                localHand.addCard(newCard);
                i++;
            }
        }
        hand = localHand;
        return hand;
    }
}
